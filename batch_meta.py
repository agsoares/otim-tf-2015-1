import os

in_folder = "data"
out_folder = "output_meta"
timeout = 3600
times_to_run = 10

files = [
  "brock400_3.clq.wcnf",
  "brock800_1.clq.wcnf",
  "c-fat200-1.clq.wcnf",
  "hamming6-2.clq.wcnf",
  "hamming8-2.clq.wcnf",
  "hamming8-4.clq.wcnf",
  "hamming10-4.clq.wcnf",
  "san400_0.5_1.clq.wcnf"
        ]

for f in files:
  for i in range(0, times_to_run):
    sys_call = ".\\bin\\main.exe -f .\\{0}\\{1} -t {2} > .\\{3}\\{1}_out{4}.txt".format(in_folder, f, timeout, out_folder, i)
    os.system(sys_call)
