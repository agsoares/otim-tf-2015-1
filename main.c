//#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct clause_t {
  int weight;
  int vars_n;
  int *vars;
} clause_t;

typedef struct result_t {
  int soft;
  int hard;
} result_t;

int *vars;
int vars_n = 0;
clause_t **clauses;
int clauses_n = 2;
int top;
int soft_n;
int hard_n;

result_t *the_solution;

int timeout = 0;
time_t start, curr;
float last_log = 0;

int neighbors_n = 5;
int flips_n = 5;
int tabu_size = 5;
int tabu_filled = 0;
int **tabu_list;


clause_t *str_to_clause(char *str) {
  clause_t *clause = malloc(sizeof(clause_t));
  char* separator = strchr(str, ' ');
  int i, spaces = 0;
  for(i = 0; str[i] != 0; i++) {
    if(str[i] == ' ')
      spaces++;
  }
  int weight = atoi(str);
  clause->weight = weight;
  clause->vars = malloc(sizeof(int)*(spaces-1));
  clause->vars_n = (spaces-1);
  if (weight == 1) {
    soft_n++;
  } else {
    hard_n++;
  }
  
  int var_n = 0;
  while (separator != 0)
  {
    *separator = 0;
    ++separator;
    int var = atoi(separator);
    if (var != 0) {
      clause->vars[var_n] = var;
      var_n++;
    }
    separator = strchr(separator, ' ');
  }
  return clause;
}

int check_clause(clause_t *clause, int *vars) {
  int j = 0, b_val = 0;
  for (j = 0; j < clause->vars_n; j++) {
      if (clause->vars[j] > 0) {
        if (vars[clause->vars[j]-1] == 1) {
          b_val = 1;
        }
      } else {
        if (vars[(clause->vars[j]*-1)-1] == 0) {
          b_val = 1;
        }
      }
      if (b_val) break;
    }
  return b_val;
}

result_t *check_solution(int *vars) {
  int i, soft = 0, hard = 0;
  #pragma omp parallel for shared(hard, soft, i)
  for (i = 0; i < clauses_n; i++) {
    int b_val = check_clause(clauses[i], vars);
    
    if (b_val) {
      if (clauses[i]->weight == top) {
        #pragma omp critical 
        {
          hard++;
        }
      } else {
        #pragma omp critical 
        {
          soft++;
        }
      }
    }
  }
  #pragma omp barrier 
  result_t *result = malloc(sizeof(result_t*));
  result->soft = soft;  result->hard = hard;
  return result;
}


void generate_solution () {
  result_t* best_result = check_solution(vars);
  result_t* result = malloc(sizeof(result_t*));
  memcpy(result, best_result,sizeof(result_t*));
  while (best_result->hard < hard_n) {
    srand(time(NULL));
    int r = rand()%vars_n;
    vars[r] = vars[r] == 0 ? 1 : 0; 
    result = check_solution(vars);
    if (result->hard >= best_result->hard) {
      free(best_result);
      best_result = result;
      printf("%d/%d hard, %d/%d soft\n",result->hard, hard_n, result->soft, soft_n);
    } else {
      free(result);
      vars[r] = vars[r] == 0 ? 1 : 0; 
    }
  }
  
}

void output () {
  time(&curr);
  float dt = difftime(curr, start);
  result_t *best_result = check_solution(vars);
  puts("=======================================================");
  printf("Time: %.2fs\n", dt);
  printf("Solution so far:\n");
  printf("%d/%d hard, %d/%d soft\n", the_solution->hard, hard_n, the_solution->soft, soft_n);
  printf("Working on:\n%d/%d hard, %d/%d soft\n",best_result->hard, hard_n, best_result->soft, soft_n);
}

int stop_contidion() {
  time(&curr);
  float dt = difftime(curr, start);
  if (last_log+10 <= dt) {
    output();
    last_log = dt;
  }
  if (timeout > 0 && dt > timeout) {
    output();
    puts("TIMEOUT!!!");
    exit(0);
  }
  return 1;
}

int check_tabu(int *solution){
  int i, j, tabu;
  for (i = 0; i < tabu_filled; i++) {
    tabu = 1;
    for (j = 0; j < vars_n; j++) {
      if (solution[j] != tabu_list[i][j]) {
        tabu = 0;
        break;
      }
    }
    if (tabu == 1) {
      return 1;
    }
  }
  
  return 0;
}

int ** generate_neighbors() {
  int **neighbors = malloc(neighbors_n*sizeof(int *));
  int i,j;
  for (i = 0; i < neighbors_n; i++) {
    int tabu_contained = 0;
    int *neighbor = NULL;
    result_t *result;
    int tabu;
    int original_flips = flips_n;
    int try = 0;
    do {
      tabu = 0;
      if (neighbor != NULL)
        free(neighbor);
      if (try > flips_n*vars_n) {
        flips_n--;
      }
      if (flips_n == 0) {
        flips_n = original_flips;
      } 
      neighbor = malloc(vars_n*sizeof(int));
      memcpy(neighbor, vars, vars_n*sizeof(int));
      for (j = 0; j < flips_n; j++) {
        srand(time(NULL));
        int r = rand()%vars_n;
        neighbor[r] = neighbor[r] == 0 ? 1 : 0;
      }
      result = check_solution(neighbor);
      tabu = check_tabu(neighbor);
      try++;
      stop_contidion();
    } while (result->hard < hard_n || tabu);
    flips_n = original_flips;
    neighbors[i] = neighbor;
  }
  return neighbors;
}

void tabu_push(int *solution) {
  int *last_solution = malloc(vars_n*sizeof(int));
  memcpy(last_solution, solution, vars_n*sizeof(int));
  if (tabu_filled == tabu_size) {
    free(tabu_list[0]);
    int i;
    for (i = 1; i < tabu_size; i++) {
      tabu_list[i-1] = tabu_list[i];
    }
    tabu_list[tabu_size-1] = last_solution;
  } else {
    tabu_list[tabu_filled] = last_solution;
    tabu_filled++;
  }
}

void tabu_search() {
  int i;
  int best_solution = -1;
  result_t *best_result = NULL;
  the_solution = check_solution(vars);
  tabu_list = malloc(tabu_size*sizeof(int *));
  while (stop_contidion()) {
    best_result = NULL;
    int **neighbors = generate_neighbors();
    best_solution = -1;
    for (i = 0; i < neighbors_n; i++) {
      result_t *result = check_solution(neighbors[i]);
      if (best_result == NULL || result->soft >= best_result->soft) {
        best_solution = i;
        best_result = result; 
        if (best_result->soft > the_solution->soft) {
          the_solution = best_result; 
        }
      }
    }
    if (best_solution >= 0 && neighbors[best_solution] != NULL) {
      memcpy(vars, neighbors[best_solution], vars_n*sizeof(int));
      tabu_push(vars);
    }
    for (i = 0; i < neighbors_n; i++) {
      free(neighbors[i]);
    }
  }
  
}


int main (int argc, char *argv[]) 
{
  //Input handling
  int i;
  FILE* file_in;
  if (argc > 1) {
    for (i = 1; i< argc; i++) {
      if (strcmp("-f", argv[i]) == 0) {
        i++;
        file_in = fopen(argv[i], "r");
      }
      if (strcmp("-t", argv[i]) == 0) {
        i++;
        timeout = atoi(argv[i]);
      }
    }
  } else {
    exit(0);
  }
  if(file_in == NULL) {
    puts("file not found");
    exit(0);
  }
  time(&start);
  //File Reading
  fseek(file_in, 0, SEEK_END);
  long fsize = ftell(file_in);
  fseek(file_in, 0, SEEK_SET);
  char *fcontent = malloc(fsize + 1);
  fread(fcontent, fsize, 1, file_in);
  fclose(file_in);
  fcontent[fsize] = 0; 
  char *line = strtok (fcontent,"\n");
  int counter = 0;
  while (line != NULL)
  {
    if (line[0] == 'c') {
    } else if(line[0] == 'p') {
      
      sscanf(line, "p wcnf %d %d %d", &vars_n, &clauses_n, &top);
      clauses = malloc(clauses_n*sizeof(clause_t *));
      vars = malloc(vars_n*sizeof(int));
      for (i = 0; i < vars_n; i++) {
        vars[i] = 1;
      }
      
    } else {
      char *str = malloc(strlen(line+1));
      memcpy(str, line, strlen(line+1));
      clause_t *clause = (clause_t *) str_to_clause(str);
      clauses[counter] = clause;
      counter++;      
    }
    line = strtok(NULL, "\n");
  }
  
  generate_solution();
  tabu_search();
  
}