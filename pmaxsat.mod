param s, integer, > 0;
param h, integer, > 0;
param n, integer, > 0;

set S{1..s};
set H{1..h};

var x{1..n}, binary;

var y{1..s}, binary, >= 0;
var k{1..h}, binary, >= 0;

s.t. c{i in 1..s}:
     sum{j in S[i]} (if j > 0 then x[j] else (1-x[-j])) >= y[i];
     
s.t. d{i in 1..h}:
     sum{j in H[i]} (if j > 0 then x[j] else (1-x[-j])) >= k[i];
     
s.t. e: sum{i in 1..h} k[i] >= h;

maximize soft: sum{i in 1..s} y[i];

solve;

printf '%s of %g hard clauses\n', sum{i in 1..h} k[i], h;
printf '%s of %g soft clauses\n', sum{i in 1..s} y[i], s;

end;