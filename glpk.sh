#!/bin/bash

FOLDER="parsed_data"
N=1
T=3600

for file in $FOLDER/*.dat
do
  for ((i=0; i<=$N; i++))
  do
    glpsol -m pmaxsat.mod -d $file --tmlim $T > $file"_out"$i".txt"
  done
done