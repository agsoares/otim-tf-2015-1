import sys

if len(sys.argv) < 2:
  exit()

hard = []
soft = []
n = 0  
file_in = open(sys.argv[1], "r")

for line in file_in:
  if (line[0] == 'c'):
    continue
  if (line[0] == 'p'):  
    n = line.split(' ')[2]
    continue
  
  clause = line.partition(' ')[2].rpartition(' ')[0]
  if (line.split(' ')[0] == '1'):
    soft.append(clause)
  else:
    hard.append(clause)

file_out = open(sys.argv[1] + ".dat", "w")
file_out.write("data;\n")
file_out.write("param s := {0};\n".format(len(soft)))
file_out.write("param h := {0};\n".format(len(hard)))
file_out.write("param n := {0};\n".format(n))
for i in range(0,len(soft)):
  file_out.write("set S[{0}] := {1};\n".format(i+1, soft[i]))

for i in range(0,len(hard)):
  file_out.write("set H[{0}] := {1};\n".format(i+1, hard[i]))

file_out.write("end;")
file_out.close()