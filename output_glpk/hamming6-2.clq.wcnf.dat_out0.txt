GLPSOL: GLPK LP/MIP Solver, v4.52
Parameter(s) specified in the command line:
 -m pmaxsat.mod -d parsed_data/hamming6-2.clq.wcnf.dat --tmlim 3600
Reading model section from pmaxsat.mod...
pmaxsat.mod:28: warning: final NL missing before end of file
28 lines were read
Reading data section from parsed_data/hamming6-2.clq.wcnf.dat...
parsed_data/hamming6-2.clq.wcnf.dat:261: warning: final NL missing before end of file
261 lines were read
Generating c...
Generating d...
Generating e...
Generating soft...
Model has been successfully generated
GLPK Integer Optimizer, v4.52
258 rows, 320 columns, 960 non-zeros
320 integer variables, all of which are binary
Preprocessing...
256 hidden packing inequaliti(es) were detected
256 rows, 128 columns, 512 non-zeros
128 integer variables, all of which are binary
Scaling...
 A: min|aij| =  1.000e+00  max|aij| =  1.000e+00  ratio =  1.000e+00
Problem data seem to be well scaled
Constructing initial basis...
Size of triangular part is 256
Solving LP relaxation...
GLPK Simplex Optimizer, v4.52
256 rows, 128 columns, 512 non-zeros
      0: obj =   0.000000000e+00  infeas =  1.920e+02 (0)
*   140: obj =   0.000000000e+00  infeas =  0.000e+00 (0)
*   282: obj =   3.200000000e+01  infeas =  0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Integer optimization begins...
+   282: mip =     not found yet <=              +inf        (1; 0)
+   282: >>>>>   3.200000000e+01 <=   3.200000000e+01   0.0% (1; 0)
+   282: mip =   3.200000000e+01 <=     tree is empty   0.0% (0; 1)
INTEGER OPTIMAL SOLUTION FOUND
Time used:   0.0 secs
Memory used: 0.7 Mb (720008 bytes)
192 of 192 hard clauses
32 of 64 soft clauses
Model has been successfully processed
